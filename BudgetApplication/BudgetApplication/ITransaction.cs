using System;

namespace BudgetApplication
{
    public interface ITransaction
    {
        DateTimeOffset Date { get; } 
        ICurrencyAmount Amount { get; }
        string ToFileString();
    }
}