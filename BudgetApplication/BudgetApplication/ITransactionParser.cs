namespace BudgetApplication
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input);
    }
}