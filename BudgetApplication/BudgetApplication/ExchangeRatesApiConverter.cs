using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace BudgetApplication
{
    public class ExchangeRatesApiConverter : ICurrencyConverter
    {
        private readonly HttpClient _httpClient;
        private readonly IMemoryCache _memoryCache;
        private string _apiKey;

        public ExchangeRatesApiConverter(HttpClient httpClient, IMemoryCache memoryCache, string apiKey)
        {
            _httpClient = httpClient;
            _memoryCache = memoryCache;
            _apiKey = apiKey;
        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            return ConvertCurrencyAsync(amount, currencyCode).Result;
        }
        
        private async Task<ICurrencyAmount> ConvertCurrencyAsync(ICurrencyAmount amount, string currencyCode)
        {
            var cachedResponse = await _memoryCache.GetOrCreateAsync("exchangeratesapi", cacheEntry =>
            {
                cacheEntry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(30);
                return GetExchangeRates();
            });
            var amountInBase = amount.Amount / (decimal) cachedResponse.Rates[amount.CurrencyCode];
            var amountInTarget = amountInBase * (decimal) cachedResponse.Rates[currencyCode];
            
            return new CurrencyAmount(currencyCode, amountInTarget);
        }
        
        

        private async Task<ExchangeRatesApiResponse> GetExchangeRates()
        {
            var response = await _httpClient.GetAsync($"http://api.exchangeratesapi.io/v1/latest?access_key={_apiKey}");
            response = response.EnsureSuccessStatusCode();
            var rates = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ExchangeRatesApiResponse>(rates);
        }
        
        
    }

    public class ExchangeRatesApiResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("rates")]
        public Dictionary<string, double> Rates { get; set; }
    }
}