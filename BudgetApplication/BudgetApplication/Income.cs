﻿using System;

namespace BudgetApplication
{
    public class Income : ITransaction
    {
        public ICurrencyAmount Amount { get; }
        public DateTimeOffset Date { get; }
        public string Source { get; }

        public Income(ICurrencyAmount amount, DateTimeOffset date, string source)
        {
            Amount = amount;
            Date = date;
            Source = source;
        }

        public override string ToString() => $"Зачисление {Amount} от {Source}";
        
        public string ToFileString() => $"Зачисление {Amount} {Source}";
    }
}
