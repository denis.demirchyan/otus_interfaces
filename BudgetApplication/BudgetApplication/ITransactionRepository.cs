namespace BudgetApplication
{
    public interface ITransactionRepository
    {
        void AddTransaction(ITransaction transaction); 
        ITransaction[] GetTransactions();
    }
}