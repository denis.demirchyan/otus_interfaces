using System;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BudgetApplication
{
    public class InFileTransactionRepository : ITransactionRepository
    {
        private readonly ITransactionParser _transactionParser;

        public InFileTransactionRepository(ITransactionParser transactionParser)
        {
            _transactionParser = transactionParser;
        }

        public void AddTransaction(ITransaction transaction)
        { 
            Task.Run(async () => await AddTransactionAsync(transaction));
        }

        private async Task AddTransactionAsync(ITransaction transaction)
        {
            await using StreamWriter file = new($"{Environment.CurrentDirectory}/TransactionRepository.txt", append: true);
            await file.WriteLineAsync(transaction.ToFileString());
        }

        public ITransaction[] GetTransactions()
        {
            List<ITransaction> transactions = new List<ITransaction>();
            foreach (string line in File.ReadLines($"{Environment.CurrentDirectory}/TransactionRepository.txt"))
            {
                transactions.Add(_transactionParser.Parse(line));
            }

            return transactions.ToArray();
        }
    }
}