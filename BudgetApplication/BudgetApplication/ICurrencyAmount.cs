namespace BudgetApplication
{
    public interface ICurrencyAmount
    {
        string CurrencyCode { get; } 
        decimal Amount { get; }
    }
}