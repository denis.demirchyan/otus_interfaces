namespace BudgetApplication
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            return new CurrencyAmount(currencyCode, amount.Amount);
        }
    }
}